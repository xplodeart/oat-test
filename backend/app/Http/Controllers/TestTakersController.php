<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class TestTakersController extends Controller {

    /*
    * Parses a single TestTaker entry from the data source
    * we use this method to parse a single test taker in case if the data source format changes in the future
    * @param Array $testTaker
    * @return Array Returns the frontend ready parsed item.
    */
    private static function parseTestTaker ( $testTaker ) {

        unset( $testTaker['password'] ); // security reasons, we dont send the password to the frontend

        return $testTaker;

    }

    public function index () {

        $dataFile = database_path() . '/_data/testtakers.json';

        $rawData = file_get_contents( $dataFile, true );

        $data = json_decode( $rawData, true );

        $refinedData = [];

        foreach ( $data as $user ) {

            $refinedData[] = self::parseTestTaker( $user );

        }

        return response()->json( $refinedData )
            ->header( 'Access-Control-Allow-Origin', '*' )
            ->header( 'Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS' );

    }

}
