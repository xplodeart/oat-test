import React, { Component } from 'react';

import logo from './logo.svg';

import './App.css';

import { Message, Table } from 'semantic-ui-react';

import UserRow from './UserRow';

class App extends Component {

    state = {

        testTakers: []

    };

    async componentWillMount () {

        try {

            let data = await fetch( 'http://localhost:8080/api/testtakers' );

            this.setState( {

                testTakers: await data.json()

            } );

        } catch ( e ) {

            console.log( e );

        }

    }

    render() {

        return (

            <div className="App">

                <header className="App-header">

                    <img src={logo} className="App-logo" alt="logo" />

                    <h1 className="App-title">TestTakers List Test</h1>

                </header>

                { this.state.testTakers.length === 0 && <Message>

                    <Message.Header>Waiting for server data...</Message.Header>

                    <p>Please make sure the backend server is running on port: 8080</p>

                </Message> }

                <div className="tableWrap">

                    { this.state.testTakers.length > 0 && <Table celled>
                    
                        <Table.Header>

                            <Table.Row>

                                <Table.HeaderCell>Image</Table.HeaderCell>

                                <Table.HeaderCell>Username</Table.HeaderCell>

                                <Table.HeaderCell>Email</Table.HeaderCell>

                                <Table.HeaderCell>Full Name</Table.HeaderCell>

                            </Table.Row>

                        </Table.Header>

                        <Table.Body>

                            { this.state.testTakers.map( ( user ) => {
                            
                                return <UserRow key={user.email} details={user} />;
                          
                            } ) }

                        </Table.Body>

                    </Table> }

                </div>

            </div>

        );

    }

}

export default App;
