import React, { Component } from 'react';

import PropTypes from 'prop-types';

import { Table, Image } from 'semantic-ui-react';

class UserRow extends Component {

    render() {

        return (

            <Table.Row>

                <Table.Cell>

                    <Image src={this.props.details.picture} rounded size='mini' />

                </Table.Cell>

                <Table.Cell>{this.props.details.login}</Table.Cell>

                <Table.Cell>{this.props.details.email}</Table.Cell>

                <Table.Cell>{this.props.details.firstname + ' ' + this.props.details.lastname}</Table.Cell>

            </Table.Row>

        );

    }

}

UserRow.propTypes = {

    details: PropTypes.object.isRequired

};

export default UserRow;
